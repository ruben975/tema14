<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220302134954 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE marturii (id INT AUTO_INCREMENT NOT NULL, prenume_autor VARCHAR(255) NOT NULL, continut VARCHAR(255) NOT NULL, data DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE marturii');
        $this->addSql('ALTER TABLE mp3_mesaje_audio CHANGE nume nume VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prenume prenume VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE titlu_mesaj titlu_mesaj VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE path path VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
