<?php

namespace App\Repository;

use App\Entity\MP3MesajeAudio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MP3MesajeAudio|null find($id, $lockMode = null, $lockVersion = null)
 * @method MP3MesajeAudio|null findOneBy(array $criteria, array $orderBy = null)
 * @method MP3MesajeAudio[]    findAll()
 * @method MP3MesajeAudio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MP3MesajeAudioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MP3MesajeAudio::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(MP3MesajeAudio $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(MP3MesajeAudio $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return MP3MesajeAudio[] Returns an array of MP3MesajeAudio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MP3MesajeAudio
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
