<?php

namespace App\Entity;

use App\Repository\MP3MesajeAudioRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MP3MesajeAudioRepository::class)]
class MP3MesajeAudio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $nume;

    #[ORM\Column(type: 'string', length: 100)]
    private $prenume;

    #[ORM\Column(type: 'string', length: 255)]
    private $titlu_mesaj;

    #[ORM\Column(type: 'string', length: 255)]
    private $path;

    #[ORM\Column(type: 'date')]
    private $data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getPrenume(): ?string
    {
        return $this->prenume;
    }

    public function setPrenume(string $prenume): self
    {
        $this->prenume = $prenume;

        return $this;
    }

    public function getTitluMesaj(): ?string
    {
        return $this->titlu_mesaj;
    }

    public function setTitluMesaj(string $titlu_mesaj): self
    {
        $this->titlu_mesaj = $titlu_mesaj;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }
}
