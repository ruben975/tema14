<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MarturiiRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MarturiiRepository::class)]
#[ApiResource]
class Marturii
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $prenume_autor;

    #[ORM\Column(type: 'string', length: 255)]
    private $continut;

    #[ORM\Column(type: 'date')]
    private $data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenumeAutor(): ?string
    {
        return $this->prenume_autor;
    }

    public function setPrenumeAutor(string $prenume_autor): self
    {
        $this->prenume_autor = $prenume_autor;

        return $this;
    }

    public function getContinut(): ?string
    {
        return $this->continut;
    }

    public function setContinut(string $continut): self
    {
        $this->continut = $continut;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }
}
